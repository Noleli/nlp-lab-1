import math, csv

# read in BNC
OpenFile = open("bncplus.txt")
bnc = {}
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    if temp[1] not in bnc:
        bnc[temp[1]] = (temp[0], temp[2], temp[3]) # (count, pos, ndocs)
    elif int(temp[0]) > int(bnc[temp[1]][0]):
        bnc[temp[1]] = (temp[0], temp[2], temp[3]) # (count, pos, ndocs)


totaldocs = 4128.0

# read in post counts, add idf
OpenFile = open("Post_logf.txt")
postwords = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    #word = temp[1]
    wordcount = float(bnc[temp[1]][0])
    ActualFinds = float(bnc[temp[1]][2])
    lambd = wordcount/totaldocs
    Poisson = math.exp(-lambd)
    ExpectedZeros = totaldocs * Poisson
    ExpectedFinds = totaldocs - ExpectedZeros
    rIDF = math.log(ExpectedFinds,2) - math.log(ActualFinds, 2)
    # idf = -1*math.log(float(bnc[temp[1]][2]))
    if rIDF == 0.0 or rIDF == -0.0:
        rIDF = int(0)
    if temp[1] == "storm":
        SurplusZeros = (totaldocs - ActualFinds) - ExpectedZeros
        print("Expected %d\t"%ExpectedFinds+"Shortage %d\t"%SurplusZeros+"rIDF %f\t"%rIDF+"storm"+"\n")
    wordDict = {'count': temp[0], 'word': temp[1], 'plogf': temp[2], 'ridf': rIDF}
    postwords.append(wordDict)

sortedpostwords = sorted(postwords, key=lambda k: k['ridf'], reverse=True)

csvWriter = csv.DictWriter(open('post_ridf.txt', 'wb'), ['count', 'word', 'plogf', 'ridf'], delimiter='\t')
csvWriter.writerows(sortedpostwords)