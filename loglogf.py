import math, csv

# read in BNC
OpenFile = open("BNCwordinfo.txt")
bnc = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    wordDict = {'count': temp[0], 'word': temp[1], 'pos': temp[2], 'ndocs': temp[3]}
    bnc.append(wordDict)

# read in Post_logf
OpenFile = open("Post_logf.txt")
post = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    wordDict = {'count': temp[0], 'word': temp[1], 'logf': temp[2]}
    post.append(wordDict)

bnctotalcounts = 0
for bncword in bnc:
    bnctotalcounts = bnctotalcounts + int(bncword['count'])
logbnctotalcounts = math.log(bnctotalcounts)
# print(bnctotalcounts)

# compare word lists
# Not using sets because I need to access associated frequencies.
overlaps = []
for postword in post:
    matchcount = 0
    wordmatches = []
    for bncword in bnc:
        if(postword['word'] == bncword['word']):
            bnclogf = math.log(float(bncword['count']))/logbnctotalcounts
            wordmatches.append({'word': postword['word'],
                            'postlogf': postword['logf'],
                            'bnclogf': bnclogf,
                            'diff': float(postword['logf']) - bnclogf
                            })
    # find the max of duplicates
    if len(wordmatches) > 0:
        while len(wordmatches) > 1:
            if float(wordmatches[0]['bnclogf']) > float(wordmatches[1]['bnclogf']):
                del wordmatches[1]
            else:
                del wordmatches[0]
    else:
        wordmatches.append({'word': postword['word'],
                        'postlogf': postword['logf'],
                        'bnclogf': 1,
                        'diff': float(postword['logf']) - 1
                        })
    overlaps.append(wordmatches[0])

csvWriter = csv.DictWriter(open('overlaps.txt', 'wb'), ['word', 'postlogf', 'bnclogf', 'diff'], delimiter='\t')
csvWriter.writerows(overlaps)
