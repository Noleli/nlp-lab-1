# Part I

par(cex.axis=.7)
# par(mfrow=c(1,3))

DrAMinusBNC <- read.delim("~/Dropbox/School/Ling/Lab 1/DrAMinusBNC.txt", header=F, quote="")
DrAMinusBNC$type <- factor(DrAMinusBNC$V2, levels=c("proper", "initialism", "error", "compound", "productive"))
plot(DrAMinusBNC$type, main="Dr. A novel word type distribution")

DrBMinusBNC <- read.delim("~/Dropbox/School/Ling/Lab 1/DrBMinusBNC.txt", header=F, quote="")
DrBMinusBNC$type <- factor(DrBMinusBNC$V2, levels=c("proper", "initialism", "error", "compound", "productive"))
plot(DrBMinusBNC$type, main="Dr. B novel word type distribution")

DrXMinusBNC <- read.delim("~/Dropbox/School/Ling/Lab 1/DrXMinusBNC.txt", header=F, quote="")
DrXMinusBNC$type <- factor(DrXMinusBNC$V2, levels=c("proper", "initialism", "error", "compound", "productive"))
plot(DrXMinusBNC$type, main="Dr. X novel word type distribution")

# Part II
# Method 1
overlaps <- read.delim("~/Dropbox/School/Ling/Lab 1/overlaps.txt", header=F, quote="")
overlaps <- overlaps[order(overlaps$V4, decreasing=TRUE),]
top100 <- overlaps[1:100,1]
plot(overlaps[1:100,2], overlaps[1:100,3], main="BNC density vs combined post density", xlab="Post density (log)", ylab="BNC density (log)")
text(overlaps[1:100,2], overlaps[1:100,3], overlaps[1:100,1], cex=0.6, pos=1)

# Method 2
bncplus <- read.delim("~/Dropbox/School/Ling/Lab 1/bncplus.txt", col.names=c("count", "word", "pos", "ndocs"), header=F, quote="")
bncplus$idf <- -log(bncplus$ndocs)
bncplus <- bncplus[order(bncplus$idf, decreasing=TRUE),]