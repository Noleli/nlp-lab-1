mykeywords = ["tropical", "cyclone", "cyclones", "hurricane", "hurricanes", "climate", "intensity", "paleotempestology", "geographical", "genesis", "cyclogenesis", "sediment", "landfalling", "climatology", "poleward"]
mykeywords = set(mykeywords)
# read in method 1 top 100
OpenFile = open("method1.txt")
method1 = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    method1.append(temp[0])
method1 = set(method1)

# read in method 2 top 100
OpenFile = open("post_idf.txt")
method2 = []
lines = OpenFile.readlines()
OpenFile.close()
for i, line in enumerate(lines):
    temp = line.split()
    method2.append(temp[1])
    if i >= 132:
        break
method2 = set(method2)

# read in method 2 top 100
OpenFile = open("post_ridf.txt")
method3 = []
lines = OpenFile.readlines()
OpenFile.close()
for i, line in enumerate(lines):
    temp = line.split()
    method3.append(temp[1])
    if i >= 99:
        break
method3 = set(method3)

m1overlap = mykeywords & method1
m2overlap = mykeywords & method2
m3overlap = mykeywords & method3

print("Method 1: " + str(len(m1overlap)) + "\nMethod 2: " + str(len(m2overlap)) + "\nMethod 3: " + str(len(m3overlap)))