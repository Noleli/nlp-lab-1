#Function to extract unique elements of a list, assuming a sorted list
def unique(Wordlist):
    Uniquelist = []
    Previous = Wordlist[0]
    Uniquelist.append(Previous)
    for word in Wordlist:
        if word == Previous:
            pass
        else:
            Uniquelist.append(word)
            Previous = word
    return Uniquelist


# Open the text frequency file
OpenFile = open("DrBCounts.txt")

# read in the lines in the file
lines = OpenFile.readlines()

#Create an empty list to put all the words into
Wordlist = []

#Put the words into the list, using tempbuffer to take apart each line
#Numbering starts from 0. We want the second element, indexed with 1. 
for line in lines:
    tempbuffer = line.split()
    Wordlist.append(tempbuffer[1])

#Open the BNC file
BNCFile = open("BNCwordinfo.txt")

#read in the lines in the file
BNClines = BNCFile.readlines()

#Create an empty list to put the BNC words into
BNClist = []

#Put the BNC words into the list, using tempbuffer to take apart each line
#We want the second of the four elements on each line
for line in BNClines:
    tempbuffer = line.split()
    BNClist.append(tempbuffer[1])


#Open an output file
Outfile = open("BNCList.txt", "w")
for word in BNClist:
    Outfile.write(word+"\n")

#Create a set of words that occur in Rasmus but not in the BNC
DrXNew = set(Wordlist) - set(BNClist)

#NB For set union, use | For set intersection use &

#Open an output file for words that are  in DrX but not in the BNC
Outfile2 = open("DrBMinusBNC.txt","w")

#Put the words into the output file
for word in DrXNew:
    Outfile2.write(word+"\n")

#Close the files
  
Outfile.close()
Outfile2.close()
OpenFile.close()



