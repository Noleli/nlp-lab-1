# BNC-Plus has 100,114,943 words, taken from 4126 documents
# The word "storm" occurs 2048 times over 813 documents. 3313 documents
# have no occurrences of "storm".

# On the average, it is expected to occur 2048/4126 = 0.5 times per document
# What is the predicted probability of zero? How does it compare to 3313?
# How does the distribution of "storm" over the documents compare to what it would be 
# in the Bag of Words model?

import math

# note use of decimal to force floating operation

lambd = 2048.0/4126.0

#Poisson counting distribution equation simplifies because lambd^0 = 1 and
# 0! also = 1.

Poisson = math.exp(-lambd)
ExpectedZeros = 4126 * Poisson

#NB A surplus of documents lacking the word is the same as a shortage of documents having the
#the word. The bigger the surplus, the more the word is concentrated.

SurplusZeros = 3313 - ExpectedZeros
ExpectedFinds = 4126 - ExpectedZeros
ActualFinds = 813.0

#Now compute rIDF using Church and Gale's formula. 

rIDF = math.log(ExpectedFinds,2) - math.log(ActualFinds, 2)

# Print the results. %d is a format to print an integer. %f prints a floating point number. \t is a tab
# \n is a newline. %feeds a variable into an expression with a format. + concatenates.

print("Expected %d\t"%ExpectedFinds+"Shortage %d\t"%SurplusZeros+"rIDF %f\t"%rIDF+"storm"+"\n")

#Or, we can write the results to a file. Here, I've used parens to make a set of variables to feed to
#a series of formats

outfile = open("RIDF", "w")
outfile.write("%d\t%d\t%f\t "%(ExpectedFinds, SurplusZeros, rIDF) +"storm"+"\n")
outfile.close()

#Now let's look at some limiting cases to make sure we understood the math
#Case one: Target word occurs once per document, for all the documents in our corpus.
#What do you expect to see?

lambd = 4126/4126.0
Poisson = math.exp(-lambd)
ExpectedZeros = 4126 * Poisson
SurplusZeros = 0 - ExpectedZeros
ExpectedFinds = 4126 - ExpectedZeros
ActualFinds = 4126
rIDF = math.log(ExpectedFinds,2) - math.log(ActualFinds, 2)
print("Expected %d\t"%ExpectedFinds+"Shortage %d\t"%SurplusZeros+"rIDF %f\t"%rIDF+"uniform"+"\n")

#Case Two: Target word has the same count of 4126, but all examples are piled up in one document.
#What do you expect to see?

lambd = 4126/4126.0
Poisson = math.exp(-lambd)
ExpectedZeros = 4126 * Poisson
SurplusZeros = 4125 - ExpectedZeros
ExpectedFinds = 4126 - ExpectedZeros
ActualFinds = 1
rIDF = math.log(ExpectedFinds,2) - math.log(ActualFinds, 2)
print("Expected %d\t"%ExpectedFinds+"Shortage %d\t"%SurplusZeros+"rIDF %f\t"%rIDF+"concentrated"+"\n")

#Case Three: What should we see if the word is less frequent, but all examples are still piled up
#in one document?

lambd = 41/4126.0
Poisson = math.exp(-lambd)
ExpectedZeros = 4126 * Poisson
SurplusZeros = 4125 - ExpectedZeros
ExpectedFinds = 4126 - ExpectedZeros
ActualFinds = 1
rIDF = math.log(ExpectedFinds,2) - math.log(ActualFinds, 2)
print("Expected %d\t"%ExpectedFinds+"Shortage %d\t"%SurplusZeros+"rIDF %f\t"%rIDF+"rare_concentrated"+"\n")


















