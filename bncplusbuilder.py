import csv

#Function to extract unique elements of a list, assuming a sorted list
def unique(Wordlist):
    Uniquelist = []
    Previous = Wordlist[0]
    Uniquelist.append(Previous)
    for word in Wordlist:
        if word == Previous:
            pass
        else:
            Uniquelist.append(word)
            Previous = word
    return Uniquelist

# return a list of dict entries with word, count, filename
def makewordbag(filename):
    OpenFile = open(filename)
    lines = OpenFile.readlines()
    Wordlist = []
    for line in lines:
        tempbuffer = line.split()
        for word in tempbuffer:
            Wordlist.append(word)
    Sortedlist = [word for word in Wordlist]
    Sortedlist.sort()
    Typelist = unique(Sortedlist)
    wordbag = []
    for word in Typelist:
        wordbag.append({'word': word, 'count': Wordlist.count(word), 'file': filename})
    return wordbag

# read in BNC
OpenFile = open("BNCwordinfo.txt")
bnc = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    wordDict = {'count': temp[0], 'word': temp[1], 'pos': temp[2], 'ndocs': temp[3]}
    bnc.append(wordDict)

# read in additional files
files = ["DrA.txt", "DrB.txt"]
filewords = []
for thefile in files:
    filewords.append(makewordbag(thefile))

for thefile in filewords:
    for worddict in thefile:
        wordmatches = []
        nmatches = 0
        for j, bncword in enumerate(bnc):
            if(worddict['word'] == bncword['word']):
                wordmatches.append({'addr': j, 'count': bncword['count']})
                nmatches = nmatches + 1
        # find the max of duplicates
        if len(wordmatches) > 0:
            while len(wordmatches) > 1:
                if int(wordmatches[0]['count']) > int(wordmatches[1]['count']):
                    del wordmatches[1]
                else:
                    del wordmatches[0]
            bnc[wordmatches[0]['addr']]['count'] = int(bnc[wordmatches[0]['addr']]['count']) + int(worddict['count'])
            bnc[wordmatches[0]['addr']]['ndocs'] = int(bnc[wordmatches[0]['addr']]['ndocs']) + 1
        else:
            bnc.append({'count': int(worddict['count']), 'word': worddict['word'], 'pos': '??', 'ndocs': 1})

csvWriter = csv.DictWriter(open('bncplus.txt', 'wb'), ['count', 'word', 'pos', 'ndocs'], delimiter='\t')
csvWriter.writerows(bnc)