import math

#Function to extract unique elements of a list, assuming a sorted list
def unique(Wordlist):
    Uniquelist = []
    Previous = Wordlist[0]
    Uniquelist.append(Previous)
    for word in Wordlist:
        if word == Previous:
            pass
        else:
            Uniquelist.append(word)
            Previous = word
    return Uniquelist


# Open the text file
OpenFile = open("ABcombined.txt")

# read in the lines in the file
lines = OpenFile.readlines()

#Create an empty list to put all the words into
Wordlist = []

#Put the words into the list, using tempbuffer to take apart each line
#Notice the : in the for construction and the indentation; if you do not
#do this correctly, you will get an error message for your loop.
for line in lines:
    tempbuffer = line.split()
    for word in tempbuffer:
        Wordlist.append(word)

#Sort the word list by making a copy of it, and sorting the copy
Sortedlist = [word for word in Wordlist]
Sortedlist.sort()

#Use the sorted list to create a list of word types
Typelist = unique(Sortedlist)

#Create a dictionary to tabulate statistics about each word type
# Note use of curly brackets to create the dictionary.

CountsDict = {}
logfDict = {}

logN = math.log(9048)

#Put in entries

for word in Typelist:
    CountsDict[word] = Wordlist.count(word)
    logfDict[word] = math.log(Wordlist.count(word))/logN

#Open an output file. Save the output into the file.
Outfile = open("Post_logf.txt", "w")
for word in Typelist[0:-1]:
    Outfile.write("%d"%CountsDict[word]+"\t"+word+"\t{}".format(logfDict[word])+"\n")


#Close the files. If you don't do this, the buffer will not finish writing out!
Outfile.close()
OpenFile.close()



