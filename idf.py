import math, csv

# read in BNC
OpenFile = open("bncplus.txt")
bnc = {}
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    if temp[1] not in bnc:
        bnc[temp[1]] = (temp[0], temp[2], temp[3]) # (count, pos, ndocs)
    elif int(temp[0]) > int(bnc[temp[1]][0]):
        bnc[temp[1]] = (temp[0], temp[2], temp[3]) # (count, pos, ndocs)

# read in post counts, add idf
OpenFile = open("Post_logf.txt")
postwords = []
lines = OpenFile.readlines()
OpenFile.close()
for line in lines:
    temp = line.split()
    #word = temp[1]
    #ndocs = bnc[temp[1]][2]
    idf = -1*math.log(float(bnc[temp[1]][2]))
    if idf == 0.0 or idf == -0.0:
        idf = int(0)
    wordDict = {'count': temp[0], 'word': temp[1], 'plogf': temp[2], 'idf': idf}
    postwords.append(wordDict)

sortedpostwords = sorted(postwords, key=lambda k: k['idf'], reverse=True)

csvWriter = csv.DictWriter(open('post_idf.txt', 'wb'), ['count', 'word', 'plogf', 'idf'], delimiter='\t')
csvWriter.writerows(sortedpostwords)